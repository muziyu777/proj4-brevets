# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

The implementation that you will do will fill in times as the input fields are filled using Ajax and Flask. Currently the miles to km (and vice versa) is implemented with Ajax. You'll extend that functionality as follows:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* You'll also implement the logic in acp_times.py based on the algorithm given above. I will leave much of the design to you. You'll turn the implementation that you do. See below for more information.

## Testing

A suite of nose test cases is a requirement of this project. Design the test cases based on an interpretation of rules here (https://rusa.org/pages/acp-brevet-control-times-calculator). Be sure to test your test cases: You can use the current brevet time calculator (https://rusa.org/octime_acp.html) to check that your expected test outputs are correct. While checking these values once is a manual operation, re-running your test cases should be automated in the usual manner as a Nose test suite.

To make automated testing more practical, your open and close time calculations should be in a separate module. Because I want to be able to use my test suite as well as yours, I will require that module be named acp_times.py and contain the two functions I have included in the skeleton code (though revised, of course, to return correct results).

We should be able to run your test suite by changing to the "brevets" directory and typinng "nosetests". All tests should pass. You should have at least 5 test cases, and more importantly, your test cases should be chosen to distinguish between an implementation that correctly interprets the ACP rules and one that does not.

## README
author:Bowen Hou
email:bhou@uoregon.edu
Source:
This project is base on ACP webpage

asset:
including acp_time.py,flask_brevet.py and some basic code
Using
to use it, we need a docker, which is a container
Use command line "docker build .", then "docker run -p5000:5000 image_name"
use"nosetest" can check the test.py

Rules: 
Base on ACP page(https://rusa.org/pages/acp-brevet-control-times-calculator), we know that this is a time calculator for bike sporters, we use US time zone to make it and use arrow to get local time.
then when we find the open_time, which is the rule for bikers about the fastest finish time, and close_time, the rule about the slowest finish time.
Because it may through different control point, different point bikers will have different rule for speed, we use a general rule to limit bikers speed.
With different rule, we will have some different for boundary, for example, 200km in two list, if one may ride 205km, the time he need may same as 200km.
There also consider two things:1. is the close time, if start point control, we will get 1 hours for close time;2. if control distance same as brevet distance,
we will get the time directly to make sure it not has error for close point.

the output will be the time you choose add the time brevet use.


## Tasks

The code under "brevets" can serve as a starting point. It illustrates a very simple Ajax transaction between the Flask server and javascript on the web page. At present the server does not calculate times. It just returns double the number of miles. Other things may be missing; add them as needed. As before, you should fork and then clone the bitbucket repository, make your changes, and turn in the URL of your repository.

You'll turn in your credentials.ini using which we will get the following:

* The working application.

* A README.md file that includes not only identifying information (your name, email, etc.) but but also a revised, clear specification of the brevet controle time calculation rules.

* An automated 'nose' test suite.

* Dockerfile

## Grading Rubric

* If your code works as expected: 100 points. This includes:
	* AJAX in the frontend. That is, open and close times are automatically populated, 
	* Logic in the backend (acp_times.py), 
	* Frontend to backend interaction (with correct requests/responses), 
	* README is updated with a clear specification, and 
	* All five tests pass.

* If the AJAX logic is not working, 10 points will be docked off. 

* If the README is not clear or missing, up to 15 points will be docked off. 

* If the test cases fail, up to 15 points will be docked off. 

* If the logic in the acp_times.py file is wrong or is missing in the appropriate location, 30 points will be docked off.

* If none of the functionalities work, 30 points will be given assuming 
    * The credentials.ini is submitted with the correct URL of your repo, and
    * The Dockerfile builds without any errors
    
* If the Dockerfile doesn't build or is missing, 10 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.
"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
#key:24

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', -1, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    #add two checkpoint 1 if people not input number
    if km ==-1:
        result={"wrongmsg":1}
        return  flask.jsonify(result = result)
    #add distance and make the larger number no more than 120%,if larger ,get second message
    distance= request.args.get('distance')
    distance = float(distance)
    maxdistance = distance *1.2
    if km > maxdistance :
        result={"wrongmsg":2}
        return flask.jsonify(result=result)
    
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    alltime = (begin_date+ " "+begin_time)
  
    
    
    #add a new input for result:wrongmsg,0 means nothing to output
    open_time = acp_times.open_time(km, distance,alltime)
    close_time = acp_times.close_time(km, distance, alltime)
    result = {"open": open_time, "close": close_time, "wrongmsg":0}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

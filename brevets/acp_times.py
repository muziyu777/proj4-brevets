"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

#base on webpage, we know that there are 3 speeds may happen
#openspeed is the max speed for open function, close speed is the min of close function
#on website, we can find some odd things, use a new directionary to write
#number's meaning of odd: when brevet distance equal to control distance:minutes, for example,200 be 13*60+30 =810minutes
openspeed={200:34, 400:32, 600:30, 1000:28, 1300:26}
closespeed={600:15, 1000:11.428, 1300:13.333}
odd ={1000:4500, 600:2400, 400:1620, 300:1200, 200:810}

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    #make a var names opentime, flash_distance is the distance after minus control_dist
    opentime =0
    flash_distance=0
    #check if the control distance larger than brevet, if yes, we can make calcultor
    if control_dist_km>brevet_dist_km:
        control_dist_km = brevet_dist_km
    while control_dist_km >0:
        for dis in openspeed:
            #control_dist larger than distance on the directory in openspeed, then time add, control distance minus
            #flash will be the distance in directory
            if control_dist_km>dis:
                opentime+=(dis-flash_distance)/openspeed[dis]
                control_dist_km-=(dis-flash_distance)
                flash_distance = dis
            #if not ,add the last time and control distance will be 0(finish)
            else:
                opentime += control_dist_km / openspeed[dis]
                control_dist_km = 0
                break
    #get time, calculate time, move time to brevet_start_time and make time zone be west of US
    hrs = int(opentime)
    mins = int((opentime*60)%60)
    secs = int(((opentime*60)*60)%60)
    dt = arrow.get(brevet_start_time,'YYYY-MM-DD HH:mm')
    dt = dt.shift(hours=+hrs,minutes=+mins)
    dt = dt.replace(tzinfo='US/Pacific')
    #if second > 30, add 1 minute
    if secs >30:
    
        dt = dt.shift(minutes=+1)
                
    
 
    return dt.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    #almost same as the open_time
    closetime =0
    flash_distance=0

    if control_dist_km>brevet_dist_km:
        control_dist_km = brevet_dist_km
    while control_dist_km >0:
        for dis in closespeed:
            if control_dist_km>dis:
                closetime+=(dis-flash_distance)/closespeed[dis]
                control_dist_km-=(dis-flash_distance)
                flash_distance = dis
            else:
                closetime += control_dist_km / closespeed[dis]
                control_dist_km = 0
                break
    hrs = int(closetime)
    mins = int((closetime*60)%60)
    secs = int(((closetime*60)*60)%60)
    #check two special situation, when control distance is 0 at the beginning, hours will be 1
    #hours, control_distance same as brevet distance, use odd directory.
    #those two things we can find on the webpage:oddities
    if control_dist_km == 0:
        hrs=1
    if control_dist_km == brevet_dist_km:
        hrs =0
        secs =0
        mins = odd[brevet_dist_km]
    dt = arrow.get(brevet_start_time,'YYYY-MM-DD HH:mm')
    dt = dt.shift(hours=+hrs,minutes=+mins)
    dt = dt.replace(tzinfo='US/Pacific')
    
    if secs >30:
        dt = dt.shift(minutes=+1)
    return dt.isoformat()
